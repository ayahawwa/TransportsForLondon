import * as React from 'react';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import TextField from '@mui/material/TextField';  
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';
import Container from '@mui/material/Container';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ListItemButton from '@mui/material/ListItemButton';
import { ListItemText } from '@material-ui/core';
import {DarkModeOutlined, PriorityHighOutlined, PedalBike, Directions} from '@mui/icons-material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Alert } from '@mui/material';
import logo from './logo.png'

function Copyright(props) {
  return (
    <Typography fontSize={8} variant="body2" color="text.secondary" align="center" {...props}>
      {'All Rights Reserved ComCast '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
);

const mdTheme = createTheme();

function Content() {
  const [open, setOpen] = React.useState(true);
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState(null);
  const [tflServices, setTflServices] = React.useState([]);
  const [bikePoints, setBikePoints] =  React.useState([]);
  const [filterBy, setFilterBy] =  React.useState(null);
  const [cycleHire, setCycleHire] = React.useState(false);
  const [selectedTflService, setSelectedTflService] =  React.useState(null);

  React.useEffect(() => {
    fetch("https://api.tfl.gov.uk/Line/Mode/tube,overground,dlr/Status?detail=true")
      .then(res => res.json())
      .then(
        (result) => {
          setTflServices(result);
          setIsLoading(false);
        },
        (error) => {
          setError(error);
        }
      )
      
      fetch("https://api.tfl.gov.uk/BikePoint/Search?query=regent")
      .then(res => res.json())
      .then(
        (result) => {
          setBikePoints(result);
          setIsLoading(false);
        },
        (error) => {
          setError(error);
        }
      )
      
  }, [])

  const toggleDrawer = () => {
    setOpen(!open);
  };
  const handleClick = (event, key) => {
      setCycleHire(false);
      setSelectedTflService(tflServices?.filter(x=>x.id === key)[0]);
  };

  const showCycleHire = () => {
      setCycleHire(true);
      };

    const handleSearch = (event, value) => {
      setFilterBy(value);
      };
  
if(error){
  return (
    <Alert severity="error">Something went wrong.</Alert>
  )
}
else if(isLoading){
  return(
    <Alert severity="info">Loading...</Alert>
  )
}
else{
  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: 'flex', bgColor:'#114091' }}>
        <CssBaseline />
        <AppBar position="absolute" sx={{ bgcolor:"#114091"}} open={open}>
          <Toolbar
            sx={{
              pr: '24px',
              bgColor:"#114091"
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: '36px',
                ...(open && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              STATUS UPDATES
            </Typography>
            <Stack spacing={2} sx={{ width: 300 }}>

          </Stack>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1]
            }}
          >
          <img src={logo} alt=""></img>&nbsp;<span style={{color:"#114091", fontSize:11, fontWeight:"bold"}}> TRANSPORT FOR LONDON</span>
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          
          <Divider />
          <List component="nav" >
            {tflServices?.sort((a,b) => (a.modeName, a.name) - (b.modeName, b.name)).map((item) =>(
            <ListItemButton key={item.id} color="action" onClick={(e)=>handleClick(e, item.id)} title={item.name}>
              {open && <ListItemText key={item.id} value={item.name} primary={item.name}
                primaryTypographyProps={{
                  color: 'textSecondary',
                    fontWeight: 'medium',
                    variant: 'body2'
                }}
              />}
                {(item?.serviceTypes?.filter(x=> x.name?.toLowerCase().includes("night")))[0]?.name? "":
                   open && <DarkModeOutlined  fontSize="small" color="primary" /> 
                   
              }
                {(item?.lineStatuses?.filter(x=> x.statusSeverity !== 10))[0]?
                 open && <PriorityHighOutlined fontSize="small" color="error"/>:""
              }
              {!open && <Directions fontSize="small" color="action" margin="0 auto"/>}
            </ListItemButton>
                ))}
              <Divider/>
            <ListItemButton key="cycleHire" onClick={showCycleHire}>
              {open && <ListItemText key="cycleHire" primary="Cycle Hire"      
              primaryTypographyProps={{
                  color: 'textSecondary',
                    fontWeight: 'medium',
                    variant: 'body2',
                }} />}
                <PedalBike fontSize="small" color="warning"/>
            </ListItemButton>

          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 100,
            height: '100vh',
            overflow: 'auto'
          }}
        >

          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <>
          {cycleHire && 
          <>
          <div style={{marginBottom:25}}>
          <h2 style={{color:"#114091", textAlign:"center"}}>Cycle Hire</h2>
          </div>
          <div style={{marginBottom:25}}>
            <Autocomplete
        freeSolo
        id="free-solo-2-demo"
        options={bikePoints?.map((bikePoint) => bikePoint.id + " " + bikePoint.commonName + " (" + bikePoint.lat + ", " + bikePoint.lon + ")")}
        onChange={handleSearch}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search input"
            InputProps={{
              ...params.InputProps,
              type: 'search',
            }}
          />
        )}
          />
          </div>
          <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow sx={{bgcolor:'#d3ebfe'}}>
                    <TableCell align="left">Id</TableCell>
                    <TableCell align="left">Common Name</TableCell>
                    <TableCell align="left">Latitude</TableCell>
                    <TableCell align="left">Longitude</TableCell>
                    <TableCell align="left">URL</TableCell>
                    <TableCell align="left">Place Type</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {filterBy && bikePoints.filter(x=> filterBy.includes(x.id)|| filterBy.includes(x.commonName) || filterBy.includes(x.lat.toString()) || filterBy.includes(x.lon.toString())
                || x.commonName.toLowerCase().includes(filterBy) || x.id.toLowerCase().includes(filterBy) || x.lat.toString().includes(filterBy) || x.lon.toString().includes(filterBy))?
                  bikePoints?.filter(x=>filterBy.includes(x.id)|| filterBy.includes(x.commonName) || filterBy.includes(x.lat.toString()) || filterBy.includes(x.lon.toString())
                || x.commonName.toLowerCase().includes(filterBy) || x.id.toLowerCase().includes(filterBy) || x.lat.toString().includes(filterBy) || x.lon.toString().includes(filterBy)).length > 0?
                    bikePoints?.filter(x=>filterBy.includes(x.id)|| filterBy.includes(x.commonName) || filterBy.includes(x.lat.toString()) || filterBy.includes(x.lon.toString())
                || x.commonName.toLowerCase().includes(filterBy) || x.id.toLowerCase().includes(filterBy) || x.lat.toString().includes(filterBy) || x.lon.toString().includes(filterBy)).map((bikePoint) => {
                    return(
                      <TableRow
                          key={bikePoint.id}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                        >
                          <TableCell component="th" scope="row">
                            {bikePoint.id}
                          </TableCell>
                          <TableCell align="left">{bikePoint.commonName}</TableCell>
                        <TableCell align="left">{bikePoint.lat}</TableCell>
                        <TableCell align="left">{bikePoint.lon}</TableCell>
                        <TableCell align="left">{bikePoint.url}</TableCell>
                        <TableCell align="left">{bikePoint.placeType}</TableCell>
                      </TableRow>
                      )
                  }):<TableRow><TableCell align="left">No bike points found for {filterBy}</TableCell></TableRow>
                : bikePoints?.map((bikePoint) => {
                    return(
                      <TableRow
                          key={bikePoint.id}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell component="th" scope="row">
                            {bikePoint.id}
                          </TableCell>
                          <TableCell align="left">{bikePoint.commonName}</TableCell>
                        <TableCell align="left">{bikePoint.lat}</TableCell>
                        <TableCell align="left">{bikePoint.lon}</TableCell>
                        <TableCell align="left">{bikePoint.url}</TableCell>
                        <TableCell align="left">{bikePoint.placeType}</TableCell>
                      </TableRow>
                      )
                  }) 
                }
                </TableBody>
              </Table>
            </TableContainer>    
            </>   
            }  
            {!cycleHire && selectedTflService &&
            <>
            <div style={{marginBottom:25}}>
              <h2 style={{color:"#114091", textAlign:"center"}}>{selectedTflService?.name}</h2>
            </div>
            {
                  selectedTflService?.lineStatuses?.filter(x=>x.statusSeverity !== 10).length > 0 &&
                  <div style={{marginBottom: 15}}>
                  <Alert severity="error">Service currently suffering disruptions</Alert>
                  </div>
                }
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow sx={{bgcolor:'#d3ebfe'}}>
                    <TableCell align="left">Disruption Description</TableCell>
                    <TableCell align="left">Status Severity</TableCell>
                    <TableCell align="left">Status Severity Desc</TableCell>
                    <TableCell align="left">Place Type</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {
                  selectedTflService?.lineStatuses?.filter(x=>x.statusSeverity !== 10).length > 0? 
                  selectedTflService?.lineStatuses.filter(x=>x.statusSeverity !== 10).map((lineStatus, index) => {
                    return(
                      <TableRow
                          key={selectedTflService?.id + index}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                        >
                        <TableCell component="th" scope="row">{lineStatus.reason}</TableCell>
                        <TableCell align="left">{lineStatus.statusSeverity}</TableCell>
                        <TableCell align="left">{lineStatus.statusSeverityDescription}</TableCell>
                        <TableCell align="left">{lineStatus.placeType}</TableCell>
                      </TableRow>
                      )
                    })
                  
                  :<TableRow>
                    <TableCell colSpan={4} align="left"><Alert severity="success">No service disruptions</Alert></TableCell>
                    </TableRow>
                }
                </TableBody>
              </Table>
            </TableContainer>    
           
            </>   
            }  
            {!selectedTflService && !cycleHire &&
              <Alert severity="info" style={{margin:"0 auto"}}>Welcome to our site! Kindly, select a service for more information.</Alert>
            }
        </>
         
           <Copyright sx={{ position:"fixed", margin:"0 auto", bottom:0, left:"50%" }} />
          </Container>
        </Box>
        
      </Box>
    </ThemeProvider>
  )};
}

export default function Landing() {
  return <Content />;
}
